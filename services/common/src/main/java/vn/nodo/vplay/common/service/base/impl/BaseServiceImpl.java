package vn.nodo.vplay.common.service.base.impl;

import org.springframework.cache.annotation.CacheEvict;
import vn.nodo.vplay.rest.service.base.BaseService;

public abstract class BaseServiceImpl implements BaseService {

    @CacheEvict(allEntries = true)
    @Override
    public void evictCache() {
        // just evict cache
    }
}
