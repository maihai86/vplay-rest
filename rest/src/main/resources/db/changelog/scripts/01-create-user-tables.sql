-- liquibase formatted sql
-- changeset haimt:01

create table tea_team
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    name        varchar(255)          not null,
    description text,
    address     text,
    invite_code varchar(255),
    PRIMARY KEY (id)
);

create table tea_team_attribute
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    team_id     bigint                not null,
    code        varchar(255)          not null,
    name        varchar(255),
    value       text,
    value_type  varchar(255),
    description text,
    PRIMARY KEY (id),
    CONSTRAINT tea_team_attr_fk1 FOREIGN KEY (team_id) REFERENCES tea_team (id)
);

create table adm_user
(
    id            bigint auto_increment not null,
    created_at    timestamp,
    created_by    bigint,
    modified_at   timestamp,
    modified_by   bigint,
    is_deleted    bool,
    is_system     bool DEFAULT 0,
    type          varchar(255)          not null,
    username      varchar(50)           not null,
    password      varchar(200),
    full_name     varchar(255),
    mobile_number varchar(50),
    email         varchar(50),
    address       text,
    gender        int,
    id_number     varchar(255),
    avatar_url    varchar(255),
    status        int,
    PRIMARY KEY (id)
);

create table adm_role
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    code        varchar(50)           not null,
    name        varchar(255),
    description text,
    PRIMARY KEY (id)
);

create table adm_permission
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    code        varchar(50)           not null,
    name        varchar(255),
    description text,
    PRIMARY KEY (id)
);

create table adm_role_permission
(
    role_id       bigint not null,
    permission_id bigint not null,
    PRIMARY KEY (role_id, permission_id),
    CONSTRAINT adm_role_permission_fk1 FOREIGN KEY (role_id) REFERENCES adm_role (id),
    CONSTRAINT adm_role_permission_fk2 FOREIGN KEY (permission_id) REFERENCES adm_permission (id)
);

create table adm_user_role
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    user_id     bigint                not null,
    role_id     bigint                not null,
    team_id     bigint,
    PRIMARY KEY (id),
    CONSTRAINT adm_user_role_fk1 FOREIGN KEY (user_id) REFERENCES adm_user (id),
    CONSTRAINT adm_user_role_fk2 FOREIGN KEY (role_id) REFERENCES adm_role (id),
    CONSTRAINT adm_user_role_fk3 FOREIGN KEY (team_id) REFERENCES tea_team (id)
);

create table adm_image_url
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    object_type varchar(255)          not null,
    object_id   bigint,
    code        varchar(255)          not null,
    url         text,
    description text,
    PRIMARY KEY (id),
    INDEX adm_image_url_idx1 (object_type, object_id)
);

create table adm_user_token
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    user_id     bigint                not null,
    token_type  varchar(255),
    token       varchar(255)          not null,
    description text,
    PRIMARY KEY (id),
    CONSTRAINT adm_user_token_fk1 FOREIGN KEY (user_id) REFERENCES adm_user (id)
);

create table adm_user_team_attribute
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    user_id     bigint                not null,
    team_id     bigint                not null,
    code        varchar(255)          not null,
    name        varchar(255),
    value       text,
    value_type  varchar(255),
    description text,
    PRIMARY KEY (id),
    CONSTRAINT adm_user_team_attr_fk1 FOREIGN KEY (user_id) REFERENCES adm_user (id),
    CONSTRAINT adm_user_team_attr_fk2 FOREIGN KEY (team_id) REFERENCES tea_team (id)
);

create table adm_notification_config
(
    id                bigint auto_increment not null,
    created_at        timestamp,
    created_by        bigint,
    modified_at       timestamp,
    modified_by       bigint,
    is_deleted        bool,
    notification_type varchar(255),
    object_type       varchar(255),
    object_id         bigint,
    title             varchar(255),
    body              text,
    is_sent           boolean,
    reference         text,
    PRIMARY KEY (id)
);

create table adm_notification
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    config_id   bigint,
    user_id     bigint,
    title       varchar(255),
    body        text,
    destination varchar(255),
    is_read     boolean,
    PRIMARY KEY (id),
    CONSTRAINT adm_notification_fk1 FOREIGN KEY (config_id) REFERENCES adm_notification_config (id),
    CONSTRAINT adm_notification_fk2 FOREIGN KEY (user_id) REFERENCES adm_user (id)
);

create table adm_comment
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    user_id     bigint                not null,
    team_id     bigint,
    type        varchar(255)          not null,
    object_type varchar(255),
    object_id   bigint,
    title       text,
    comment     text,
    vote_star   float,
    PRIMARY KEY (id),
    CONSTRAINT adm_comment_fk1 FOREIGN KEY (user_id) REFERENCES adm_user (id),
    CONSTRAINT adm_comment_fk2 FOREIGN KEY (team_id) REFERENCES tea_team (id)
);