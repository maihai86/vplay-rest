package vn.nodo.vplay.rest.repository.admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.rest.entity.admin.NotificationConfig;

@Repository
public interface NotificationConfigRepository extends JpaRepository<NotificationConfig, Long> {
}
