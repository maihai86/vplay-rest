package vn.nodo.vplay.rest.controller.base;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import springfox.documentation.annotations.ApiIgnore;
import vn.nodo.vplay.rest.entity.base.BaseEntity;
import vn.nodo.vplay.rest.response.AppResponseDto;
import vn.nodo.vplay.rest.security.token.CustomUsernamePasswordAuthenticationToken;
import vn.nodo.vplay.rest.security.userdetails.CustomUserDetails;
import vn.nodo.vplay.rest.service.base.BaseCrudService;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

public abstract class BaseCrudController<T extends BaseEntity, ID extends Serializable, A extends Serializable,
        E extends Serializable, S extends Serializable> {

    public abstract BaseCrudService<T, ID, A, E, S> getService();

    @ApiOperation("Thêm")
    @PostMapping
    public AppResponseDto<T> create(@ApiIgnore @AuthenticationPrincipal OAuth2Authentication principal, @Valid @RequestBody A data) {
        return AppResponseDto.from(getService().create(((CustomUsernamePasswordAuthenticationToken) principal.getUserAuthentication()).getId(), data));
    }

    @ApiOperation("Sửa")
    @PutMapping("{id}")
    public AppResponseDto<T> update(@ApiIgnore @AuthenticationPrincipal OAuth2Authentication principal, @PathVariable("id") ID id,
                                    @Valid @RequestBody E data) {
        return AppResponseDto.from(getService().update(((CustomUsernamePasswordAuthenticationToken) principal.getUserAuthentication()).getId(), id, data));
    }

    @ApiOperation("Chi tiết")
    @GetMapping("{id}")
    public AppResponseDto<T> detail(@PathVariable("id") ID id) {
        return AppResponseDto.from(getService().findById(id));
    }

    @ApiOperation("Danh sách phân trang")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Số trang (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Số bản ghi 1 trang", defaultValue = "20"),
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Xắp sếp kết quả theo format: property(,asc|desc). "
                            + "Xắp sếp mặc định là tăng dần (asc). "
                            + "Hỗ trợ danh sách xắp sếp 1 lúc.",
                    defaultValue = "createdAt,desc")})
    @GetMapping
    public ResponseEntity<PageImpl<T>> page(@Valid S searchData, @ApiIgnore Pageable pageable) {
        return ResponseEntity.ok(getService().findAll(searchData, pageable));
    }

    @ApiOperation("Danh sách")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Xắp sếp kết quả theo format: property(,asc|desc). "
                            + "Xắp sếp mặc định là tăng dần (asc). "
                            + "Hỗ trợ danh sách xắp sếp 1 lúc.",
                    defaultValue = "createdAt,desc")})
    @GetMapping("list")
    public ResponseEntity<List<T>> list(@Valid S searchData, @ApiIgnore Sort sort) {
        return ResponseEntity.ok(getService().findAll(searchData, sort));
    }

    @ApiOperation("Xoá")
    @DeleteMapping("{id}")
    public AppResponseDto<Boolean> delete(@ApiIgnore @AuthenticationPrincipal OAuth2Authentication principal,
                                          @PathVariable("id") ID id) {
        return AppResponseDto.from(getService().delete(((CustomUsernamePasswordAuthenticationToken) principal.getUserAuthentication()).getId(), id));
    }
}
