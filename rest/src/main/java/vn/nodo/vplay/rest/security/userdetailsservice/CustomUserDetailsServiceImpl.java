package vn.nodo.vplay.rest.security.userdetailsservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import vn.nodo.vplay.rest.repository.admin.UserRepository;
import vn.nodo.vplay.rest.entity.admin.User;
import vn.nodo.vplay.rest.security.userdetails.CustomUserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username == null) throw new UsernameNotFoundException("username can not be empty.");
        User user = userRepository.findFirstByUsernameAndTypeAndStatusOrderByCreatedAtDesc(
                username, User.UserType.VPLAY.getValue(), User.UserStatus.ACTIVE.getValue())
                .orElseThrow(() -> new UsernameNotFoundException(
                        messageSource.getMessage("error.admin.user-not-found-or-invalid-password", null, Locale.getDefault())));
        return fromUser(user);
    }

    @Override
    public UserDetails loadUserById(Long id) throws UsernameNotFoundException {
        if (id == null) throw new UsernameNotFoundException("ID can not be empty.");
        User user = userRepository.findByIdAndTypeAndStatus(id, User.UserType.VPLAY.getValue(), User.UserStatus.ACTIVE.getValue())
                .orElseThrow(() -> new UsernameNotFoundException(
                        messageSource.getMessage("error.admin.user-not-found-or-invalid-password", null, Locale.getDefault())));
        return fromUser(user);
    }

    public static CustomUserDetails fromUser(User user) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        if (user.getUserRoles() != null) {
            authorities.addAll(user.getUserRoles().stream()
                    .map(userRole -> new SimpleGrantedAuthority(userRole.getRole().getCode().trim()))
                    .collect(Collectors.toSet()));
            authorities.addAll(user.getUserRoles().stream().map(userRole -> userRole.getRole().getPermissions())
                    .flatMap(Collection::stream).collect(Collectors.toSet()).stream()
                    .map(permission -> new SimpleGrantedAuthority(permission.getCode().trim()))
                    .collect(Collectors.toSet()));
        }

        return CustomUserDetails.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .authorities(authorities)
                .build();
    }
}
