package vn.nodo.vplay.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.common.entity.admin.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findFirstByUsernameAndTypeOrderByCreatedAtDesc(String username, Integer type);

    Optional<User> findFirstByUsernameAndTypeAndStatusOrderByCreatedAtDesc(String username, Integer type, int status);

    Optional<User> findByIdAndType(Long id, Integer type);

    Optional<User> findByIdAndTypeAndStatus(Long id, Integer type, int status);
}
