package vn.nodo.vplay.admin.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vn.nodo.vplay.common.dto.base.BaseDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@ToString
public class UserAddDto extends BaseDto {

    @NotBlank(message = "Số điện thoại không được để trống.")
    @Pattern(regexp = "^(\\+84|0|02|\\+842)[0-9]{9}$", message = "Số điện thoại sai định dạng.")
    private String username;

    @NotBlank(message = "Mật khẩu không được để trống.")
    @Size(min = 6, message = "Mật khẩu phải có độ dài tối thiểu là 6 ký tự.")
    private String password;

}
