package vn.nodo.vplay.rest.service.admin.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.nodo.vplay.rest.dto.admin.PermissionAddDto;
import vn.nodo.vplay.rest.dto.admin.PermissionEditDto;
import vn.nodo.vplay.rest.dto.admin.PermissionSearchDto;
import vn.nodo.vplay.rest.repository.admin.PermissionRepository;
import vn.nodo.vplay.rest.security.userdetails.CustomUserDetails;
import vn.nodo.vplay.rest.service.admin.PermissionService;
import vn.nodo.vplay.rest.entity.admin.Permission;
import vn.nodo.vplay.rest.exception.AppException;
import vn.nodo.vplay.rest.service.base.impl.BaseCrudServiceImpl;
import vn.nodo.vplay.rest.utils.Util;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class PermissionServiceImpl extends BaseCrudServiceImpl<Permission, Long, PermissionAddDto, PermissionEditDto, PermissionSearchDto> implements PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Override
    public JpaRepository<Permission, Long> getRepository() {
        return permissionRepository;
    }

    @Override
    public Permission create(Long userId, PermissionAddDto data) {
        String code = data.getCode().trim();
        if (!code.startsWith("PERM_")) {
            throw AppException.builder().errorCodes(Collections.singletonList("error.admin.invalid-permission-code")).build();
        }

        if (permissionRepository.findFirstByCodeOrderByCreatedAtDesc(data.getCode()).isPresent()) {
            throw AppException.builder()
                    .errorCodes(Collections.singletonList("error.admin.permission-already-existed"))
                    .build();
        }
        Permission permission = Permission.builder().createdBy(userId).modifiedBy(userId).build();
        BeanUtils.copyProperties(data, permission);
        permission.setCode(code);

        return permissionRepository.save(permission);
    }

    @Override
    public Permission update(Long userId, Long id, PermissionEditDto data) {
        Permission role = permissionRepository.findById(id).orElseThrow(() -> AppException.builder()
                .errorCodes(Collections.singletonList("error.admin.permission-not-found"))
                .build());
        BeanUtils.copyProperties(data, role, Util.getNullPropertyNames(data));
        role.setModifiedBy(userId);

        return permissionRepository.save(role);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PageImpl<Permission> findAll(PermissionSearchDto data, Pageable pageable) {
        Page<Permission> page = permissionRepository.findAll(pageable);
        return new PageImpl<>(page.getContent(), page.getPageable(), page.getTotalElements());
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Permission> findAll(PermissionSearchDto data, Sort sort) {
        return permissionRepository.findAll(sort);
    }
}
