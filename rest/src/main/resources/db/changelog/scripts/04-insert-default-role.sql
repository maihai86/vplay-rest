-- liquibase formatted sql
-- changeset haimt:04

INSERT INTO adm_role (created_at, modified_at, is_deleted, code, name) VALUES (now(),now(),false,'ROLE_SUPER_ADMIN','ROLE_SUPER_ADMIN');
INSERT INTO adm_role (created_at, modified_at, is_deleted, code, name) VALUES (now(),now(),false,'ROLE_USER','ROLE_USER');