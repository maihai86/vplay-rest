package vn.nodo.vplay.rest.utils;

import java.util.*;
import java.util.stream.*;

/**
 * Codility.
 * https://github.com/Mickey0521/Codility
 * https://github.com/ZRonchy/Codility
 */
public class Solution {

    static Solution solution = new Solution();
    static java.util.Random random = new java.util.Random(System.currentTimeMillis());

    /*// BinaryGap
    public int solution(int N) {
        String strBinary = Integer.toBinaryString(N);
        System.out.println(String.format("binary of %s is: %s", N, strBinary));
        char prev = 0;
        int start = -1, binaryGap = 0;
        for (int i = 0; i < strBinary.length(); i++) {
            if (prev == '1' && strBinary.charAt(i) == '0') {
                start = i;
            }
            if (prev == '0' && strBinary.charAt(i) == '1' && start > 0 && binaryGap < (i - start)) {
                binaryGap = i - start;
            }
            prev = strBinary.charAt(i);
        }
        return binaryGap;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println("9: " + solution.solution(9));
        System.out.println("529: " + solution.solution(529));
        System.out.println("1041: " + solution.solution(1041));
        System.out.println("32: " + solution.solution(32));
    }*/

    /*// CyclicRotation
    public int[] solution(int[] A, int K) {
        if (A.length <= 0 || K <= 0) return A;
        int temp = A[A.length - 1];

        for (int i = A.length - 1; i >= 1; i--) {
            A[i] = A[i - 1];
        }
        A[0] = temp;
        if (K == 1) return A;
        return solution(A, K - 1);
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println("[3, 8, 9, 7, 6], 3: " + Arrays.toString(solution.solution(new int[]{3, 8, 9, 7, 6}, 3)));
        System.out.println("[0, 0, 0], 1: " + Arrays.toString(solution.solution(new int[]{0, 0, 0}, 1)));
        System.out.println("[1, 2, 3, 4], 4: " + Arrays.toString(solution.solution(new int[]{1, 2, 3, 4}, 4)));
        System.out.println("[1], 4: " + Arrays.toString(solution.solution(new int[]{1}, 5)));
        System.out.println("[3, 8, 9, 7, 6], 15: " + Arrays.toString(solution.solution(new int[]{3, 8, 9, 7, 6}, 15)));
    }*/

    /*// OddOccurrencesInArray
    public int solution(int[] A) {
        java.util.Arrays.sort(A);
        for (int i = 0; i <= A.length; i = i + 2) {
            if (i >= A.length) return A[A.length - 1];
            if (i + 1 < A.length && A[i] != A[i + 1]) return A[i];
        }
        return A[A.length - 1];
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println("[9,3,9,3,9,7,9]: " + solution.solution(new int[]{9,3,9,3,9,7,9}));
        System.out.println("[1,2,3,4,5,4,3,2,1]: " + solution.solution(new int[]{1,2,3,4,5,4,3,2,1}));
    }*/

    /*// FrogJmp
    public int solution(int X, int Y, int D) {
        if (X >= Y) return 0;
        return (Y - X) % D == 0 ? (Y - X) / D : (Y - X) / D + 1;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println("10, 85, 30: " + solution.solution(10, 85, 30));
        System.out.println("10, 850, 30: " + solution.solution(10, 850, 30));
        long currentMillis = System.currentTimeMillis();
        System.out.println("10, 850, 2: " + solution.solution(10, 850, 2));
        System.out.println("time out: " + (System.currentTimeMillis() - currentMillis));
    }*/

    /*// PermMissingElem
    public int solution(int[] A) {
        java.util.Arrays.sort(A);
        for (int i = 0; i < A.length; i++) {
            if (i == A.length - 1) return A[A.length - 1] + 1;
            if (A[i] + 1 != A[i + 1]) return A[i] + 1;
        }
        return A.length == 0 ? 1 : A[A.length - 1] + 1;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println("[1,2,3,4,6,7,8]: " + solution.solution(new int[]{1,2,3,4,6,7,8}));
        System.out.println("[1,2,3,5]: " + solution.solution(new int[]{1,2,3,5}));
        System.out.println("[1]: " + solution.solution(new int[]{1}));
        System.out.println("[1,2]: " + solution.solution(new int[]{1,2}));
    }*/

    /*// TapeEquilibrium
    public int solution(int[] A) {
        int N = A.length;

        int sum1 = A[0];
        int sum2 = 0;
        int P = 1;
        for (int i = P; i < N; i++) {
            sum2 += A[i];
        }
        int diff = Math.abs(sum1 - sum2);

        for (; P < N-1; P++) {
            sum1 += A[P];
            sum2 -= A[P];

            int newDiff = Math.abs(sum1 - sum2);
            if (newDiff < diff) {
                diff = newDiff;
            }
        }
        return diff;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println("[3,1,2,4,3]: " + solution.solution(new int[]{3,1,2,4,3}));
        System.out.println("[1,2]: " + solution.solution(new int[]{1,2}));
        System.out.println("[-1000, 1000]: " + solution.solution(new int[]{-1000, 1000}));
        Random random = new Random(System.currentTimeMillis());
        int[] A = new int[10000];
        for (int i = 0; i < A.length; i++) {
            A[i] = random.nextInt(100);
        }
        long millis = System.currentTimeMillis();
        System.out.println("A: " + solution.solution(A));
        System.out.println("A time: " + (System.currentTimeMillis() - millis) + " ms");
        A = new int[100000];
        for (int i = 0; i < A.length; i++) {
            A[i] = random.nextInt(1000);
        }
        millis = System.currentTimeMillis();
        System.out.println("A: " + solution.solution(A));
        System.out.println("A time: " + (System.currentTimeMillis() - millis) + " ms");
    }*/

    /*// FrogRiverOne
    public int solution(int X, int[] A) {
        int[]counter = new int[X+1];
        int ans = -1;
        int x = 0;

        for (int i=0; i<A.length; i++){
            if (counter[A[i]] == 0){
                counter[A[i]] = A[i];
                x += 1;
                if (x == X){
                    return i;
                }
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println("5, [1,3,1,4,2,3,5,4]: " + solution.solution(5, new int[]{1,3,1,4,2,3,5,4}));
    }*/

    /*// Distinct
    public int solution(int[] A) {
        *//*return (int) java.util.Arrays.stream(A).distinct().count();*//*
        java.util.Arrays.sort(A);
        if (A.length == 0) return 0;
        int start = A[0], result = 1;
        for (int i = 1; i < A.length; i++) {
            if (start != A[i]) {
                result++;
                start = A[i];
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("[2,1,1,2,3,1]: " + solution.solution(new int[]{2,1,1,2,3,1}));
        System.out.println("[1,1,2,3,4,5]: " + solution.solution(new int[]{1,1,2,3,4,5}));
        System.out.println("[]: " + solution.solution(new int[]{}));
        int[] A = new int[10000];
        for (int i = 0; i < A.length; i++) {
            A[i] = random.nextInt(100000);
        }
        long millis = System.currentTimeMillis();
        System.out.println("A: " + solution.solution(A));
        System.out.println("A time: " + (System.currentTimeMillis() - millis) + " ms");
    }*/

    /*// MaxProductOfThree
    public int solution(int[] A) {
        java.util.Arrays.sort(A);
        return Math.max(A[0] * A[1] * A[A.length - 1], A[A.length - 1] * A[A.length - 2] * A[A.length - 3]);
    }

    public static void main(String[] args) {
        System.out.println("[-3,1,2,-2,5,6]: " + solution.solution(new int[]{-3,1,2,-2,5,6}));
        System.out.println("[-5, 5, -5, 4]: " + solution.solution(new int[]{-5, 5, -5, 4}));
        int[] A = new int[100000];
        for (int i = 0; i < A.length; i++) {
            A[i] = random.nextInt(1000) - 2000;
        }
        long millis = System.currentTimeMillis();
        System.out.println("A: " + solution.solution(A));
        System.out.println("A time: " + (System.currentTimeMillis() - millis) + " ms");
    }*/

    /*// NumberOfDiscIntersections
    public int solution(int[] A) {
        int j = 0;
        Pair[] arr = new Pair[A.length * 2];
        for (int i = 0; i < A.length; i++) {
            Pair s = new Pair((long)i - (long)A[i], true);
            arr[j] = s;
            j++;
            Pair e = new Pair((long)i + (long)A[i], false);
            arr[j] = e;
            j++;
        }
        java.util.Arrays.sort(arr, new Pair(0, true));

        long numIntersect = 0;
        long currentCount = 0;
        for (Pair p: arr) {
            if (p.start) {
                numIntersect += currentCount;
                if (numIntersect > 10000000) {
                    return -1;
                }
                currentCount++;
            } else {
                currentCount--;
            }
        }

        return (int) numIntersect;
    }

    private static class Pair implements java.util.Comparator<Pair> {
        private long x;
        private boolean start;
        public Pair(long x, boolean start) {
            this.x = x;
            this.start = start;
        }

        public int compare(Pair p1, Pair p2) {
            if (p1.x < p2.x) {
                return -1;
            } else if (p1.x > p2.x) {
                return 1;
            } else {
                if (p1.start && p2.start == false) {
                    return -1;
                } else if (p1.start == false && p2.start) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("[1,5,2,1,4,0]: " + solution.solution(new int[]{1,5,2,1,4,0}));
        int[] A = new int[100000];
        for (int i = 0; i < A.length; i++) {
            A[i] = random.nextInt(Integer.MAX_VALUE);
        }
        long millis = System.currentTimeMillis();
        System.out.println("A: " + solution.solution(A));
        System.out.println("A time: " + (System.currentTimeMillis() - millis) + " ms");
    }*/

    /*// Triangle
    public int solution(int[] A) {
        java.util.Arrays.sort(A);

        for (int i = 0; i < A.length - 2; i++) {
            if (((long) A[i] + (long) A[i + 1] > A[i + 2]) &&
                    ((long) A[i + 1] + (long) A[i + 2] > A[i]) &&
                    ((long) A[i] + (long) A[i + 2] > A[i + 1])) {
                return 1;
            }
        }
        return 0;
    }*/

    /*// MaxDoubleSliceSum
    public int solution(int[] A) {
        int[] slice1LocalMax = new int[A.length];
        int[] slice2LocalMax = new int[A.length];

        //start from i=1 because slice can't start at index 0
        for (int i = 1; i < A.length - 1; i++) {
            slice1LocalMax[i] = Math.max(slice1LocalMax[i - 1] + A[i], 0);
        } //start from i=A.length-2 because slice can't end at index A.length-1
        for (int i = A.length - 2; i > 0; i--) {
            slice2LocalMax[i] = Math.max(slice2LocalMax[i + 1] + A[i], 0);
        }

        int maxDoubleSliceSum = 0;

        //compute sums of all slices to find absolute max
        for (int i = 1; i < A.length - 1; i++) {
            maxDoubleSliceSum = Math.max(maxDoubleSliceSum, slice1LocalMax[i - 1] + slice2LocalMax[i + 1]);
        }

        return maxDoubleSliceSum;
    }*/

    /*// Flags
    public int solution(int[] A) {
        // write your code in Java SE 8
        int[] peaks = new int[A.length];
        int peakStart = 0;
        int peakEnd = 0;

        //Find the peaks.
        //We don't want to traverse the array where peaks hasn't started, yet,
        //or where peaks doesn't occur any more.
        //Therefore, find start and end points of the peak as well.
        for(int i = 1; i < A.length-1; i++) {
            if(A[i-1] < A[i] && A[i+1] < A[i]) {
                peaks[i] = 1;
                peakEnd = i + 1;
            }
            if(peakStart == 0) {
                peakStart = i;
            }
        }

        int x = 1;
        //The maximum number of flags can be √N
        int limit = (int)Math.ceil(Math.sqrt(A.length));
        int prevPeak = 0;
        int counter = 0;
        int max = Integer.MIN_VALUE;

        while(x <= limit) {
            counter = 0;
            prevPeak = 0;
            for(int y = peakStart; y < peakEnd; y++) {
                //Find the peak points when we have x number of flags.
                if(peaks[y] == 1 && (prevPeak == 0 || x <= (y - prevPeak))) {
                    counter++;
                    prevPeak = y;
                }
                //If we don't have any more flags stop.
                if(counter == x ) {
                    break;
                }
            }
            //if the number of flags set on the peaks starts to reduce stop searching.
            if(counter <= max) {
                return max;
            }
            //Keep the maximum number of flags we set on.
            max = counter;
            x++;
        }
        return max;
    }*/

    /*// Peaks
    public int solution(int[] A) {
        // write your code in Java SE 8
        int count = 0;
        int N = A.length;
        int sqrtN = (int) Math.sqrt(N);
        int[] factors = new int[sqrtN * 2];
        for (int i = 1; i <= sqrtN; i++)
        {
            if (N % i == 0)
            {
                factors[count++] = i;
                factors[count++] = N / i;
            }
        }

        int i, m, all = 0, max = 0;
        for (int j = 0; j < count; j++)
        {
            int division = factors[j];
            if (division == N) continue;

            for (int k = 0; k < division; k++)
            {
                all = 0;
                m = N / division;
                for (int it = 0; it < m; it++)
                {
                    i = k * m + it;
                    if (i == 0 || i == N - 1) continue;
                    if (A[i] > A[i - 1] && A[i] > A[i + 1])
                        all++;
                }

                if (all == 0)
                    break;
            }

            if (all != 0)
            {
                if (division > max)
                    max = division;
            }
        }

        return max;
    }*/

    /*// CountFactors
    public int solution(int N) {
        int factors = 0;
        int squareRootN = (int) Math.sqrt(N);
        if(Math.pow(squareRootN, 2) != N) {
            squareRootN++; //round up for any non-perfect squares
        }
        else { //perfect squares have an additional factor
            factors++;
        }

        for(int i=1; i<squareRootN; i++) {
            if(N % i == 0) {
                factors += 2;
            }
        }

        return factors;
    }*/

    /*// MinPerimeterRectangle
    public int solution(int N) {
        int squareRootN = (int) Math.sqrt(N);

        int factor2 = 0;
        int perimeter = 0;
        int minPerimeter = Integer.MAX_VALUE;

        if(Math.pow(squareRootN, 2) != N) {
            squareRootN++; //round up for any non-perfect squares
        }
        else { //perfect square root won't be reached inside loop so calculate and set min perimeter
            minPerimeter = 2 * (squareRootN + squareRootN);
        }

        for(int i=1; i<squareRootN; i++) {
            if(N % i == 0) {
                //calculate 2nd factor by simple division since know 1st factor and N
                factor2 = N / i;
                perimeter = 2 * (factor2 + i);
                minPerimeter = Math.min(perimeter, minPerimeter);
            }
        }
        return minPerimeter;
    }*/

    /*// FibFrog
    public int solution(int[] A) {

        // note: cannot use "List" (both java.util.* and java.awt.* have "List")
        java.util.ArrayList<Integer> fibonacci = new java.util.ArrayList<>();
        fibonacci.add(0); // note: f(0) = 0 (as in the quesion)
        fibonacci.add(1);
        // note: using "while" is better than "for" (avoid errors)
        while(true){
            int temp1 = fibonacci.get( fibonacci.size()-1 );
            int temp2 = fibonacci.get( fibonacci.size()-2 );
            fibonacci.add( temp1 + temp2 );

            // if already bigger than length, then break;
            if(temp1 + temp2 > A.length){
                break;
            }
        }

        // reverse "List": from big to small
        java.util.Collections.reverse(fibonacci);

        // use "queue" with "point"
        // point(x,y) = point("position", "number of steps")
        java.util.ArrayList<java.awt.Point> queue = new java.util.ArrayList<>();
        queue.add( new java.awt.Point(-1, 0) ); // position:-1, steps:0

        // index: the current index for queue element
        int index=0;
        while(true){
            // cannot take element from queue anymore
            if(index == queue.size() ){
                return -1;
            }

            // take element from queue
            java.awt.Point current = queue.get(index);

            // from big to small
            for(Integer n: fibonacci){
                int nextPosition = current.x + n;

                // case 1: "reach the other side"
                if(nextPosition == A.length){
                    // return the number of steps
                    return current.y + 1;
                }

                // case 2: "cannot jump"
                // note: nextPosition < 0 (overflow, be careful)
                else if( (nextPosition > A.length) || (nextPosition < 0)|| (A[nextPosition]==0) ){
                    // note: do nothing
                }

                // case 3: "can jump" (othe cases)
                else{
                    // jump to next position, and step+1
                    java.awt.Point temp = new java.awt.Point(nextPosition, current.y + 1);
                    // add to queue
                    queue.add(temp);

                    A[nextPosition] = 0; // key point: for high performance~!!
                }
            }

            index++; // take "next element" from queue
        }
    }*/

    /*// Ladder
    public int[] solution(int[] A, int[] B) {

        // The task is to find out the number of ways
        // someone can climb up a ladder of N rungs
        // by ascending one or two rungs at a time.
        // It is not very hard to see that
        // this number is just the "Fibonacci number of order N"

        // we implemented an easy dynamic programming approach
        // to compute Fibonacci numbers, this will take complexity O(n)

        // I use binary operators to keep track of "N modulo 2^{30}"
        // otherwise. the Fibonacci numbers will cause a memory overflow (be careful~!!)
        // and we are also asked to return "numbers modulo some power of 2"

        int L = A.length;

        // determine the "max" for Fibonacci
        int max = 0;
        for (int i = 0; i < L; i++) {
            max = Math.max(A[i], max);
        }
        //max += 2; // for Fibonacci

        int[] fibonacci = new int[max+1]; // plus one for "0"

        // initial setting of Fibonacci (importnat)
        fibonacci[0] =1;
        fibonacci[1] =1;

        for(int i=2; i<= max; i++){
            fibonacci[i] = (fibonacci[i-1] + fibonacci[i-2]) % (1 << 30);
            // we want to find the result of "a number modulo 2^P"
            // if we first let the number modulo 2^Q (Q > P)
            // then, modulo 2^P, the esult is the same.
            // So, "we first modulo 2^30" to avoid overflow
            // where, 2^30 == 1 << 30
        }

        // to find "results"
        int[] results = new int[L];

        for(int i=0; i<L; i++){
            results[i] = fibonacci[A[i]] % (1 << B[i]); // where, "1 << B[i]" means 2^B[i]
        }

        return results;
    }*/

    /*// MinMaxDivision
    public int solution(int K, int M, int[] A) {

        // main idea:
        // The goal is to find the "minimal large sum"
        // We use "binary search" to find it (so, it can be fast)

        // We assume that the "min max Sum" will be
        // between "min" and "max", ecah time we try "mid"

        int minSum =0;
        int maxSum =0;
        for(int i=0; i<A.length; i++){
            maxSum = maxSum + A[i];          // maxSum: sum of all elements
            minSum = Math.max(minSum, A[i]); // minSum: at least one max element
        }

        int possibleResult = maxSum; // the max one must be an "ok" result

        // do "binary search" (search for better Sum)
        while(minSum <= maxSum){
            // define "mid" (binary search)
            int midSum = (minSum + maxSum) /2;

            // check if it can be divided by using
            // the minMaxSum = "mid", into K blocks ?
            boolean ok = checkDivisable(midSum, K, A);

            // if "ok", means that we can try "smaller"
            // otherwise ("not ok"), we have to try "bigger"
            if(ok == true){
                possibleResult = midSum; // mid is "ok"
                // we can try "smaller"
                maxSum = midSum - 1;
            }
            else{ // "not ok"
                // we have to try "bigger"
                minSum = midSum + 1;
            }
            // go back to "binary search" until "min > max"
        }

        return possibleResult;
    }

    // check if it can be divided by using the minMaxSum = "mid", into K blocks ?
    public boolean checkDivisable(int mid, int k, int[] a){
        int numBlockAllowed = k;
        int currentBlockSum = 0;

        for(int i=0; i< a.length; i++){
            currentBlockSum = currentBlockSum + a[i];

            if(currentBlockSum > mid){ // means: need one more block
                numBlockAllowed--;
                currentBlockSum = a[i]; // note: next block
            }

            if(numBlockAllowed == 0){
                return false; // cannot achieve minMaxSum = "mid"
            }
        }

        // can achieve minMaxSum = "mid"
        return true;
    }*/

    /*// NailingPlanks
    public int solution(int[] A, int[] B, int[] C) {
        // the main algorithm is that getting the minimal index of nails which
        // is needed to nail every plank by using the binary search
        int N = A.length;
        int M = C.length;
        // two dimension array to save the original index of array C
        int[][] sortedNail = new int[M][2];
        for (int i = 0; i < M; i++) {
            sortedNail[i][0] = C[i];
            sortedNail[i][1] = i;
        }
        // use the lambda expression to sort two dimension array
        java.util.Arrays.sort(sortedNail, (int x[], int y[]) -> x[0] - y[0]);
        int result = 0;
        for (int i = 0; i < N; i++) {//find the earlist position that can nail each plank, and the max value for all planks is the result
            result = getMinIndex(A[i], B[i], sortedNail, result);
            if (result == -1)
                return -1;
        }
        return result + 1;
    }
    // for each plank , we can use binary search to get the minimal index of the
    // sorted array of nails, and we should check the candidate nails to get the
    // minimal index of the original array of nails.
    public int getMinIndex(int startPlank, int endPlank, int[][] nail, int preIndex) {
        int min = 0;
        int max = nail.length - 1;
        int minIndex = -1;
        while (min <= max) {
            int mid = (min + max) / 2;
            if (nail[mid][0] < startPlank)
                min = mid + 1;
            else if (nail[mid][0] > endPlank)
                max = mid - 1;
            else {
                max = mid - 1;
                minIndex = mid;
            }
        }
        if (minIndex == -1)
            return -1;
        int minIndexOrigin = nail[minIndex][1];
        //find the smallest original position of nail that can nail the plank
        for (int i = minIndex; i < nail.length; i++) {
            if (nail[i][0] > endPlank)
                break;
            minIndexOrigin = Math.min(minIndexOrigin, nail[i][1]);
            // we need the maximal index of nails to nail every plank, so the
            // smaller index can be omitted
            if (minIndexOrigin <= preIndex)
                return preIndex;
        }
        return minIndexOrigin;
    }*/

    /*// MinAbsSumOfTwo
    public int solution(int[] A) {
        int N = A.length;
        java.util.Arrays.sort(A);
        int tail = 0;
        int head = N - 1;
        int minAbsSum = Math.abs(A[tail] + A[head]);
        while (tail <= head) {
            int currentSum = A[tail] + A[head];
            minAbsSum = Math.min(minAbsSum, Math.abs(currentSum));
            // If the sum has become
            // positive, we should know that the head can be moved left
            if (currentSum <= 0)
                tail++;
            else
                head--;
        }
        return minAbsSum;
    }*/

    /*// MinAbsSum
    public int solution(int[] A) {
        int arrayLength = A.length;
        int M = 0;
        for (int i = 0; i < arrayLength; i++) {
            A[i] = Math.abs(A[i]);
            M = Math.max(A[i], M);
        }

        int S = sum(A);
        int[] dp = new int[S + 1];
        int[] count = new int[M + 1];
        for (int i = 0; i < arrayLength; i++) {
            count[A[i]] += 1;
        }
        java.util.Arrays.fill(dp, -1);
        dp[0] = 0;
        for (int i = 1; i < M + 1; i++) {
            if (count[i] > 0) {
                for(int j = 0; j < S; j++) {
                    if (dp[j] >= 0) {
                        dp[j] = count[i];
                    } else if (j >= i && dp[j - i] > 0) {
                        dp[j] = dp[j - i] - 1;
                    }
                }
            }
        }
        int result = S;
        for (int i = 0; i < Math.floor(S / 2) + 1; i++) {
            if (dp[i] >= 0) {
                result = Math.min(result, S - 2 * i);
            }
        }
        return result;
    }

    public static int sum(int[] array) {
        int sum = 0;
        for(int i : array) {
            sum += i;
        }

        return sum;
    }*/

    /*// NumberSolitaire
    public int solution(int[] A) {

        // main idea:
        // using "dynamic programming" to build up the solution
        // (bottom up)

        int[] dp = new int[A.length];
        dp[0] = A[0];

        // build up from "dp[1], dp[2], ..., dp[A.length-1]"
        for(int i=1; i<A.length; i++){

            // keep the biggest one
            // be very careful: could be negtive (so use Integer.MIN_VALUE)
            int max = Integer.MIN_VALUE;

            // a die could be "1 to 6"
            for(int die=1; die<=6; die++){
                if( i-die >= 0){
                    // very important: not "A[i-die]+A[i]"
                    // instead, have to use "dp[i-die]+A[i]"
                    max = Math.max( dp[i-die]+A[i], max );
                    // dynamic programming:
                    // take the best:
                    // takeBest( dp[i-j] + value[j], curBest )
                }
            }
            dp[i] = max; // keep the best one as the dp value
        }

        return dp[A.length-1];
    }*/

    /*// MinAbsSumOfTwo
    public int solution(int[] A) {
        int N = A.length;
        java.util.Arrays.sort(A);
        int tail = 0;
        int head = N - 1;
        int minAbsSum = Math.abs(A[tail] + A[head]);
        while (tail <= head) {
            int currentSum = A[tail] + A[head];
            minAbsSum = Math.min(minAbsSum, Math.abs(currentSum));
            // If the sum has become
            // positive, we should know that the head can be moved left
            if (currentSum <= 0)
                tail++;
            else
                head--;
        }
        return minAbsSum;
    }*/

    /*// CommonPrimeDivisors
    public int solution(int[] A, int[] B) {
        int count = 0;
        for(int i=0;i<A.length;i++) {
            if(hasSamePrimeDivisors(A[i], B[i])){
                count++;
            }
        }
        return count;
    }

    public int gcd(int a, int b) {
        if(a % b == 0) return b;
        return gcd(b,a%b);
    }

    public boolean hasSamePrimeDivisors(int a, int b) {
        int gcdValue = gcd(a,b);
        int gcdA;
        int gcdB;
        while(a!=1) {
            gcdA = gcd(a,gcdValue);
            if(gcdA==1)
                break;
            a = a/gcdA;
        }
        if(a!=1)  {
            return false;
        }
        while(b!=1) {
            gcdB = gcd(b,gcdValue);
            if(gcdB==1)
                break;
            b = b/gcdB;
        }
        return b==1;
    }*/

    /*// CountNonDivisible
    public int[] solution(int[] A) {
        int[][] D = new int[A.length*2 + 1][2];

        for (int i = 0; i < A.length; i++) {
            D[A[i]][0]++;
            D[A[i]][1] = -1;
        }

        for (int i = 0; i < A.length; i++) {
            if (D[A[i]][1] == -1) {
                D[A[i]][1] = 0;
                for (int j = 1; j <= Math.sqrt(A[i]) ; j++) {
                    if (A[i] % j == 0 && A[i] / j != j) {
                        D[A[i]][1] += D[j][0];
                        D[A[i]][1] += D[A[i]/j][0];
                    } else if (A[i] % j == 0 && A[i] / j == j) {
                        D[A[i]][1] += D[j][0];
                    }
                }
            }
        }
        for (int i = 0; i < A.length; i++) {
            A[i] = A.length - D[A[i]][1];
        }
        return A;
    }*/

    /*// CountSemiprimes
    public int[] solution(int N, int[] P, int[] Q) {
        int length = P.length;
        int[] prime = sieve(N);
        int[] semiprime = semiprime(prime);
        int[] result = new int[length];
        int[] semiprimesAggreation = new int[N+1];

        for(int i=1;i<N+1;i++) {
            semiprimesAggreation[i] = semiprime[i];
            semiprimesAggreation[i] += semiprimesAggreation[i-1];
        }

        for(int i=0;i<length;i++) {
            result[i] = semiprimesAggreation[Q[i]] - semiprimesAggreation[P[i]] + semiprime[P[i]];
        }
        return result;
    }

    public int[] sieve(int N) {
        int[] prime = new int[N+1];
        for(int i=2; i<=(double)Math.sqrt(N); i++) {
            if(prime[i] == 0) {
                int k = i*i;
                while(k <= N) {
                    if(prime[k] == 0){
                        prime[k] = i;
                    }
                    k += i;
                }
            }
        }
        return prime;
    }

    public int[] semiprime(int[] prime) {
        int semiprime[] = new int[prime.length];
        for(int i=0;i<prime.length;i++) {
            if(prime[i] == 0) continue;
            int firstFactor = prime[i];
            if(prime[i/firstFactor] == 0) semiprime[i]=1;
        }
        return semiprime;
    }*/

    /*// MaxCounters
    public int[] solution(int N, int[] A) {
        // write your code in Java SE 8

        // 1. key point: maintain the max value
        int max = 0;

        // 2. key point: maintain the current_min (very important!!!)
        // so, we can move "the 2nd for-loop" outside "the 1st for-loop"
        // by maintaining "min"
        int min =0;

        // new integer array
        int[] my_array = new int[N];

        *//* no need to initialize (because the values are "0" by default)
        for(int i=0; i<my_array.length; i++){
            my_array[i] =0;
        }
        *//*

        for(int i=0; i<A.length; i++){
            if( A[i] >= 1 && A[i] <= N){ // normal case

                // important: check the "min" before "increasing by 1"
                if(my_array[ A[i] -1] < min){
                    my_array[ A[i] -1] = min; // update it to "min"
                }

                my_array[ A[i] -1 ] ++;  // increased by 1

                if( my_array[ A[i] -1 ] > max){ // maintain max
                    max = my_array[ A[i] -1 ];
                }
            }
            else if( A[i] == N+1){      // special case
                *//* cannot use for-loop (will take too much time)
                for(int j=0; j<my_array.length; j++){
                    my_array[j] = max;
                }
                // instead, we maintain "min", so we can move the for-loop outside *//*
                min = max; // all the values should be bigger than "max"
                // therefore, "min = max"
            }
        }

        // move the 2nd for-loop outside the 1st for-loop
        // update some elements who have not been updated yet
        for(int j=0; j<my_array.length; j++){
            if(my_array[j] < min){
                my_array[j] = min; // update it to "min"
            }
        }

        return my_array;
    }*/

    /*// MissingInteger
    public int solution(int[] A) {
        // write your code in Java SE 8

        //special case
        if(A.length ==0){
            return 1;
        }

        // Using "set" to check if an element has appeared
        // note: need to "import java.util.*" (important)
        java.util.Set<Integer> set = new java.util.HashSet<Integer>();

        // add elements into the set
        for(int i=0; i< A.length; i++){
            set.add(A[i]);
        }

        // note: the missing number is not possible bigger than (A.length)
        //       because there are only (A.length) numbers
        for(int i=1; i<= A.length; i++){
            if(set.contains(i) != true) // the 1st missing element
                return i;
        }

        // means: there are no missing numbers from 1 to A.length
        // Therefore, the missing number is "A.length+1" (important)
        return A.length+1;
    }*/

    /*// PermCheck
    public int solution(int[] A) {
        // write your code in Java SE 8

        // to check "permutation"
        // the main idea is as follows:
        // 1. use set to remember which elements have appeared
        // 2. use "for loop" to check if all the elements from "1 to A.length" appeared
        // If all the elements have appeared, then "yes".
        // Otherwise, "no".

        java.util.Set<Integer> set = new java.util.HashSet<Integer>();

        for(int i=0; i < A.length; i++){
            set.add(A[i]);
        }

        // check if "all" the elements from "1 to A.length" appeared
        for(int i=1; i<= A.length; i++){
            if( set.contains(i) == false )
                return 0; // not a permutation (A[i] is missing)
        }

        // if it contains all the elements (from "1 to A.length")
        // then, "yes"
        return 1;
    }*/

    /*// ArrayInversionCount
    public int solution(int[] A) {
        long inversionNum = countInversion(A, 0, A.length - 1);
        return inversionNum > 1000000000 ? -1 : (int) inversionNum;
    }

    long countInversion(int[] A, int begin, int end) {
        if (begin >= end) {
            return 0;
        }

        int middle = (begin + end) / 2;
        long inversionNum = countInversion(A, begin, middle)
                + countInversion(A, middle + 1, end);
        int[] merged = new int[end - begin + 1];
        int index1 = begin;
        int index2 = middle + 1;
        for (int i = 0; i < merged.length; i++) {
            if (index2 == end + 1
                    || (index1 != middle + 1 && A[index1] <= A[index2])) {
                merged[i] = A[index1];
                index1++;
            } else {
                merged[i] = A[index2];
                index2++;
                inversionNum += middle + 1 - index1;
            }
        }
        for (int i = begin; i <= end; i++) {
            A[i] = merged[i - begin];
        }
        return inversionNum;
    }*/

    /*// StrSymmetryPoint
    public int solution(String S) {
        int length = S.length();
        if (length % 2 != 0 && isPalindrome(S)) {
            return length / 2;
        } else {
            return -1;
        }
    }

    boolean isPalindrome(String S) {
        for (int i = 0, j = S.length() - 1; i < j; i++, j--) {
            if (S.charAt(i) != S.charAt(j)) {
                return false;
            }
        }
        return true;
    }*/

    /*// TreeHeight
    class Tree {
        public int x;
        public Tree l;
        public Tree r;
    }

    public int solution(Tree T) {
        return compute(T) - 1;
    }

    private int compute(Tree T) {
        if (T == null) {
            return 0;
        }
        return Math.max(compute(T.l), compute(T.r)) + 1;
    }*/

    /*// FloodDepth
    public int solution(int[] A) {
        int highestIdx = 0;
        int lowestIdx = 0;
        int max = 0;

        for (int i = 1; i < A.length; i++) {
            int current = A[i];
            int highest = A[highestIdx];
            int lowest = A[lowestIdx];
            if (current > highest) {
                max = Math.max(highest - lowest, max);
                highestIdx = i;
                lowestIdx = highestIdx;
            } else if (current > lowest) {
                max = Math.max(current - lowest, max);
            } else if (current < lowest) {
                lowestIdx = i;
            }
        }
        return max;
    }*/

    /*// LongestPassword
    public int solution(String S) {
        *//*
            https://codility.com/demo/results/trainingSCWJYU-2HE/
            Correctness: 100%
            Performance: not assessed
            Task score: 100%
        *//*
        int res = 0;
        //we form an array with the given words
        String[] strings = S.split(" ");

        //we filter out the words with non alphanumerical characters
        java.util.ArrayList<String> listWithAlphanumeric = new java.util.ArrayList<>();
        for (String s : strings) {
            if (s.matches("^[a-zA-Z0-9]*$"))
                listWithAlphanumeric.add(s);
        }

        int countLetters = 0;
        int countNumbers = 0;
        java.util.ArrayList<String> toRemove = new java.util.ArrayList<>();
        //we parse each word and count its letters and numbers
        //when letters or numbers don't meet the restrictions we add the word to arrayList toRemove
        for (String s : listWithAlphanumeric) {
            String lower = s.toLowerCase();
            countLetters = 0;
            countNumbers = 0;
            for (int i = 0; i < lower.length(); i++) {
                if ((lower.codePointAt(i) >= 97 && lower.codePointAt(i) <= 122)) {
                    countLetters++;
                    continue;
                }
                if (lower.codePointAt(i) >= 48 && lower.codePointAt(i) <= 57)
                    countNumbers++;
            }
            if (countLetters % 2 == 1 || countNumbers % 2 == 0)
                toRemove.add(s);
        }

        //we remove the words of toRemove from arrayList listWithAlphanumeric
        for (String s : toRemove) {
            for (String string : listWithAlphanumeric) {
                if (s == null ? string == null : s.equals(string)) {
                    listWithAlphanumeric.remove(string);
                    break;
                }
            }
        }

        if (listWithAlphanumeric.isEmpty())
            return -1;
        else
            //we find the longest accepted word
            for (String s : listWithAlphanumeric)
                if (s.length() > res)
                    res = s.length();

        return res;
    }*/

    /*// SlalomSkiing
    public int solution(int[] A) {
        long bound = java.util.Arrays.stream(A).max().getAsInt();
        long[] withMirrors = new long[A.length*3];
        for(int i=0; i<A.length; i++){
            withMirrors[i*3] = bound * 2 + A[i] + 1;
            withMirrors[i*3+1] = bound * 2 - A[i] + 1;
            withMirrors[i*3+2] = A[i];
        }
        return lengthOfLIS(withMirrors);
    }

    public int lengthOfLIS(long[] nums) {
        long[] temp = new long[nums.length];
        int idx = 0;
        for(int i=0; i<nums.length; i++){
            int fnd = java.util.Arrays.binarySearch(temp, 0, idx, nums[i]);

            if(fnd < 0){
                fnd = -fnd-1;
            }

            if(fnd==idx){
                idx++;
            }

            temp[fnd] = nums[i];
        }
        return idx;
    }*/

    /*// DwarfsRafting
    private int[] dwarfs;
    private int[] capacity;

    public int solution(int N, String S, String T) {
        if (N % 2 == 1) {
            return -1;
        }
        S = S.toLowerCase();
        T = T.toLowerCase();
        int[] barrels = parseString(S, N);
        dwarfs = parseString(T, N);
        capacity = new int[4];
        for (int i = 0; i < barrels.length; i++) {
            capacity[i] = (N * N / 4) - barrels[i];
        }
        int diagonal = getBalanced(0, 3);
        int antiDiagonal = getBalanced(1, 2);
        if (diagonal < 0 || antiDiagonal < 0) {
            return -1;
        }
        return diagonal + antiDiagonal - java.util.stream.IntStream.of(dwarfs).sum();
    }

    private int charToInt(char letter) {
        return letter - 'a';
    }

    private int getBalanced(int left, int right) {
        int balance = Math.min(capacity[left], capacity[right]);
        if (balance < Math.max(dwarfs[left], dwarfs[right])) {
            return -1;
        } else {
            return balance * 2;
        }
    }

    private int[] parseString(String in, int N) {
        if (in.isEmpty()) {
            return new int[4];
        }
        int[] data = new int[4];
        String[] chunks = in.split(" ");
        for (String chunk : chunks) {
            int column = Integer.parseInt(chunk.substring(0, chunk.length() - 1));
            int row = charToInt(chunk.charAt(chunk.length() - 1));
            if (column <= N / 2) {
                if (row < N / 2) {
                    data[0]++;
                } else {
                    data[1]++;
                }
            } else {
                if (row < N / 2) {
                    data[2]++;
                } else {
                    data[3]++;
                }
            }
        }
        return data;
    }*/

    /*// RectangleBuilderGreaterArea
    public int solution(int[] A, int X) {
        java.util.HashMap<Integer, Integer> map = new java.util.HashMap<>();
        for (int a : A) {
            if (map.get(a) == null) {
                map.put(a, 1);
            } else {
                map.put(a, map.get(a) + 1);
            }
        }
        java.util.List<Integer> valid = map.entrySet().stream()
                .filter(entry -> entry.getValue() >= 2)
                .map(java.util.Map.Entry::getKey)
                .collect(java.util.stream.Collectors.toList());
        int count = 0;
        int duplicatesCount = 0;
        int result = 0;
        java.util.Collections.sort(valid);
        for (Integer number : valid) {
            int factor = (int) Math.ceil((double) X / (double) number);
            int index = java.util.Collections.binarySearch(valid, factor);
            if (index < 0) {
                index = -index - 1;
            }
            count += valid.size() - index;
            if (number >= factor) {
                if (map.get(number) >= 4) {
                    duplicatesCount++;
                }
                count--;
            }
            result = count / 2 + duplicatesCount;
            if (result > 1000000000) {
                return -1;
            }
        }
        return result;
    }*/

    /*// MissingInteger
    public int solution(int[] A) {
        java.util.Set<Integer> set = new java.util.HashSet<>();

        for(int i=0; i< A.length; i++){
            set.add(A[i]);
        }

        for(int i=1; i<= A.length; i++) {
            if(!set.contains(i)) return i;
        }

        return A.length + 1;
    }

    public static void main(String[] args) {
        System.out.println("[1, 3, 6, 4, 1, 2]: " + solution.solution(new int[]{1, 3, 6, 4, 1, 2}));
        System.out.println("[1, 2, 3]: " + solution.solution(new int[]{1, 2, 3}));
        System.out.println("[−1, −3]: " + solution.solution(new int[]{-1,-3}));
        int[] A = new int[100000];
        for (int i = 0; i < A.length; i++) {
            A[i] = random.nextInt(1000000) - 2000000;
        }
        long millis = System.currentTimeMillis();
        System.out.println("A: " + solution.solution(A));
        System.out.println("A time: " + (System.currentTimeMillis() - millis) + " ms");
    }*/

    /*public int solution(String[] A) {
        List<Map<Character, Integer>> characterMap = new ArrayList<>();
        for (int i = 0; i < A.length; i++) {
            Map<Character, Integer> baseMap = new HashMap<Character, Integer>();
            char[] charArray = A[i].toCharArray();
            for (Character ch : charArray) {
                if (baseMap.containsKey(ch)) {
                    baseMap.put(ch, baseMap.get(ch) + 1);
                } else {
                    baseMap.put(ch, 1);
                }
            }
            Set<Character> keys = baseMap.keySet();
            for (Character ch : keys) {
                if (baseMap.get(ch) > 1) {
                    System.out.println(ch + "  is " + baseMap.get(ch) + " times");
                    baseMap.remove(ch);
                }
            }
            characterMap.add(baseMap);
        }

        return characterMap.stream().mapToInt(value -> value.values().stream().mapToInt(value1 -> value1).sum()).sum();
    }

    public static void main(String[] args) {
        System.out.println("\"co\", \"dil\", \"ity\": " + solution.solution(new String[]{"co", "dil", "ity"}));
        *//*int[] A = new int[100000];
        for (int i = 0; i < A.length; i++) {
            A[i] = random.nextInt(1000000) - 2000000;
        }
        long millis = System.currentTimeMillis();
        System.out.println("A: " + solution.solution(A));
        System.out.println("A time: " + (System.currentTimeMillis() - millis) + " ms");*//*
    }*/

    public int solution(String S) {
        List<Character> intList = S.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < intList.size(); i++) {
            if (intList.get(i) == 'B') {
                for (int j = i + 1; j < intList.size(); j++) {
                    if (intList.get(j) == 'A' && !positions.contains(j)) {
                        positions.add(j);
                        break;
                    }
                }
            }
        }
        return positions.size();
    }

    public static void main(String[] args) {
        System.out.println("\"BAAABAB\": " + solution.solution("BAAABAB"));
        System.out.println("\"BBABAA\": " + solution.solution("BBABAA"));
        System.out.println("\"AABBBB\": " + solution.solution("AABBBB"));
        System.out.println("\"AABBAB\": " + solution.solution("AABBAB"));
        System.out.println("\"BABBBB\": " + solution.solution("BABBBB"));
    }
}
