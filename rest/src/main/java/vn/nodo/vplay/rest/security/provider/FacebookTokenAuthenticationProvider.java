package vn.nodo.vplay.rest.security.provider;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import vn.nodo.vplay.rest.repository.admin.RoleRepository;
import vn.nodo.vplay.rest.repository.admin.UserRepository;
import vn.nodo.vplay.rest.entity.admin.Role;
import vn.nodo.vplay.rest.entity.admin.User;
import vn.nodo.vplay.rest.entity.admin.UserRole;
import vn.nodo.vplay.rest.exception.AppException;
import vn.nodo.vplay.rest.security.token.FacebookAuthenticationToken;
import vn.nodo.vplay.rest.security.userdetailsservice.CustomUserDetailsService;

import java.util.Collections;
import java.util.Optional;

@Component
public class FacebookTokenAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Override
    @Transactional
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        FacebookAuthenticationToken facebookAuthentication = (FacebookAuthenticationToken) authentication;
        String facebookUserId = facebookAuthentication.getPrincipal().toString();
        String facebookAccessToken = authentication.getCredentials().toString();

        if (!StringUtils.isBlank(facebookUserId) && !StringUtils.isBlank(facebookAccessToken)) {
            Optional<User> optionalUser = userRepository.findFirstByUsernameAndTypeOrderByCreatedAtDesc(facebookUserId,
                    User.UserType.FACEBOOK.getValue());
            Role role = roleRepository.findFirstByCodeOrderByCreatedAtDesc(Role.RoleCode.ROLE_USER.getValue())
                    .orElseThrow(() -> AppException.builder()
                            .errorCodes(Collections.singletonList("error.admin.role-not-found"))
                            .build());
            User user = null;
            if (!optionalUser.isPresent()) {
                User mUser = User.builder()
                        .type(User.UserType.FACEBOOK.getValue())
                        .username(facebookUserId)
                        .fullName(facebookAuthentication.getFullName())
                        .avatarUrl(facebookAuthentication.getAvatarUrl())
                        .email(facebookAuthentication.getEmail())
                        .build();
                mUser.setUserRoles(Collections.singletonList(UserRole.builder()
                        .role(role)
                        .user(mUser)
                        .build()));
                user = userRepository.save(mUser);
            } else {
                User mUser = optionalUser.get();
                mUser.setFullName(facebookAuthentication.getFullName());
                mUser.setAvatarUrl(facebookAuthentication.getAvatarUrl());
                mUser.setEmail(facebookAuthentication.getEmail());
                user = userRepository.save(mUser);
            }

            UserDetails loadedUser = userDetailsService.loadUserByUsername(facebookUserId);
            if (loadedUser == null) {
                throw new InternalAuthenticationServiceException(
                        "UserDetailsService returned null, which is an interface contract violation");
            }

            FacebookAuthenticationToken result = new FacebookAuthenticationToken(loadedUser, authentication.getCredentials(), loadedUser.getAuthorities());
            result.setDetails(authentication.getDetails());
            return result;
        } else {
            throw new BadCredentialsException("External system authentication failed");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(FacebookAuthenticationToken.class);
    }
}
