package vn.nodo.vplay.common.controller.base;

import org.springframework.security.access.prepost.PreAuthorize;

@PreAuthorize("denyAll()")
public interface DenyAllAuthorized {
}
