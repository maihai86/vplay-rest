package vn.nodo.vplay.rest.service.admin;

import vn.nodo.vplay.rest.dto.admin.PermissionAddDto;
import vn.nodo.vplay.rest.dto.admin.PermissionEditDto;
import vn.nodo.vplay.rest.dto.admin.PermissionSearchDto;
import vn.nodo.vplay.rest.entity.admin.Permission;
import vn.nodo.vplay.rest.service.base.BaseCrudService;

public interface PermissionService extends BaseCrudService<Permission, Long, PermissionAddDto, PermissionEditDto, PermissionSearchDto> {
}
