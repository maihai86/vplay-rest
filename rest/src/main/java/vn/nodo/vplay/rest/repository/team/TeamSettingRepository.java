package vn.nodo.vplay.rest.repository.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.rest.entity.team.TeamSetting;

@Repository
public interface TeamSettingRepository extends JpaRepository<TeamSetting, Long> {
}
