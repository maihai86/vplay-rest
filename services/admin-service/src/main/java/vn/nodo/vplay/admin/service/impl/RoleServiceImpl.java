package vn.nodo.vplay.admin.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.nodo.vplay.rest.dto.admin.RoleAddDto;
import vn.nodo.vplay.rest.dto.admin.RoleEditDto;
import vn.nodo.vplay.rest.dto.admin.RoleSearchDto;
import vn.nodo.vplay.rest.repository.admin.RoleRepository;
import vn.nodo.vplay.rest.service.admin.RoleService;
import vn.nodo.vplay.common.entity.admin.Role;
import vn.nodo.vplay.common.exception.AppException;
import vn.nodo.vplay.common.security.base.CustomUserDetails;
import vn.nodo.vplay.rest.service.base.impl.BaseCrudServiceImpl;
import vn.nodo.vplay.common.util.Util;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class RoleServiceImpl extends BaseCrudServiceImpl<Role, Long, RoleAddDto, RoleEditDto, RoleSearchDto> implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public JpaRepository<Role, Long> getRepository() {
        return roleRepository;
    }

    @Override
    public Role create(CustomUserDetails principal, RoleAddDto data) {
        String code = data.getCode().trim();
        if (!code.startsWith("ROLE_")) {
            throw AppException.builder().errorCodes(Collections.singletonList("error.admin.invalid-role-code")).build();
        }

        if (roleRepository.findFirstByCodeOrderByCreatedAtDesc(data.getCode()).isPresent()) {
            throw AppException.builder()
                    .errorCodes(Collections.singletonList("error.admin.role-already-existed"))
                    .build();
        }
        Role role = Role.builder().build();
        BeanUtils.copyProperties(data, role);
        role.setCode(code);

        return roleRepository.save(role);
    }

    @Override
    public Role update(CustomUserDetails principal, Long id, RoleEditDto data) {
        Role role = roleRepository.findById(id).orElseThrow(() -> AppException.builder()
                .errorCodes(Collections.singletonList("error.admin.role-not-found"))
                .build());
        BeanUtils.copyProperties(data, role, Util.getNullPropertyNames(data));

        return roleRepository.save(role);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PageImpl<Role> findAll(RoleSearchDto data, Pageable pageable) {
        Page<Role> page = roleRepository.findAll(pageable);
        return new PageImpl<>(page.getContent(), page.getPageable(), page.getTotalElements());
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Role> findAll(RoleSearchDto data, Sort sort) {
        return roleRepository.findAll(sort);
    }
}
