package vn.nodo.vplay.team.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.common.entity.team.FundTransaction;

@Repository
public interface FundTransactionRepository extends JpaRepository<FundTransaction, Long> {
}
