package vn.nodo.vplay.common.entity.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import vn.nodo.vplay.common.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "adm_notification")
@Where(clause = "is_deleted = false")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EntityListeners(AuditingEntityListener.class)
public class Notification extends BaseEntity {

    @Column(name = "config_id")
    private Long configId;

    @Column(name = "user_id")
    private Long userId;

    @Column
    private String title;

    @Column
    @Lob
    private String body;

    @Column
    private String destination;

    @Column(name = "is_read")
    private Boolean read;

}
