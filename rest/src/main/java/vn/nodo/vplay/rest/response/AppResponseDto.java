package vn.nodo.vplay.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import vn.nodo.vplay.rest.exception.AppException;
import vn.nodo.vplay.rest.exception.AppExceptionError;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppResponseDto<T> implements Serializable {

    @Builder.Default
    private Integer httpStatus = 200;
    private List<AppExceptionError> errors;
    private T data;

    public static AppResponseDto fromError(AppException appException) {
        return AppResponseDto.builder()
                .httpStatus(appException.getHttpStatus())
                .errors(appException.getErrors())
                .build();
    }

    public static AppResponseDto fromError(int httpStatus, Exception ex) {
        return AppResponseDto.builder()
                .httpStatus(httpStatus)
                .errors(Collections.singletonList(AppExceptionError.builder()
                        .errorCode("error.common.error")
                        .errorMessage(ex.getLocalizedMessage())
                        .stackFrames(ExceptionUtils.getStackFrames(ex))
                        .build()))
                .build();
    }

    public static AppResponseDto fromError(Exception ex) {
        return fromError(500, ex);
    }

    public static <T> AppResponseDto<T> from(T data) {
        AppResponseDto<T> responseDto = new AppResponseDto<>();
        responseDto.setData(data);
        return responseDto;
    }
}
