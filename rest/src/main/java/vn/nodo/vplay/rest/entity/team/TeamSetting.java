package vn.nodo.vplay.rest.entity.team;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import vn.nodo.vplay.rest.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "tea_team_setting")
@Where(clause = "is_deleted = false")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EntityListeners(AuditingEntityListener.class)
public class TeamSetting extends BaseEntity {

    @Column(nullable = false)
    private String type;

    @Column(name = "sub_type", nullable = false)
    private String subType;

    @Column
    private String name;

    @Column
    @Lob
    private String description;

    @Column(name = "amount_limit")
    private BigDecimal amountLimit;

    @Column(name = "training_schedule_type")
    private String trainingScheduleType;

    @Column(name = "training_schedule_time")
    private Float trainingScheduleTime;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "team_id", nullable = false)
    private Team team;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "training_pitch_id")
    private Pitch trainingPitch;

}
