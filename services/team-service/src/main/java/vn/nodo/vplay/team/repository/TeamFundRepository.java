package vn.nodo.vplay.team.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.common.entity.team.TeamFund;

@Repository
public interface TeamFundRepository extends JpaRepository<TeamFund, Long> {
}
