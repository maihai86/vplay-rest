package vn.nodo.vplay.common.entity.content;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import vn.nodo.vplay.common.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "cnt_news")
@Where(clause = "is_deleted = false")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EntityListeners(AuditingEntityListener.class)
public class News extends BaseEntity {

    @Column(nullable = false)
    private String type;

    @Column(name = "title_type")
    private String titleType;

    @Column
    @Lob
    private String title;

    @Column(name = "content_type")
    private String contentType;

    @Column
    @Lob
    private String content;

}
