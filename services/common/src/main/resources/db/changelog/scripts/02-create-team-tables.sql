-- liquibase formatted sql
-- changeset haimt:02

create table tea_pitch
(
    id             bigint auto_increment not null,
    created_at     timestamp,
    created_by     bigint,
    modified_at    timestamp,
    modified_by    bigint,
    is_deleted     bool,
    name           varchar(255)          not null,
    description    text,
    address        text,
    latitude       double,
    longitude      double,
    contact_mobile varchar(255),
    contact_name   varchar(255),
    PRIMARY KEY (id)
);

create table tea_match
(
    id              bigint auto_increment not null,
    created_at      timestamp,
    created_by      bigint,
    modified_at     timestamp,
    modified_by     bigint,
    is_deleted      bool,
    home_team_id    bigint                not null,
    away_team_id    bigint,
    pitch_id        bigint,
    intend_time     timestamp,
    available_to    timestamp,
    status          int,
    home_team_score int,
    away_team_score int,
    ended_at        timestamp,
    PRIMARY KEY (id),
    CONSTRAINT tea_match_fk1 FOREIGN KEY (home_team_id) REFERENCES tea_team (id),
    CONSTRAINT tea_match_fk2 FOREIGN KEY (away_team_id) REFERENCES tea_team (id),
    CONSTRAINT tea_match_fk3 FOREIGN KEY (pitch_id) REFERENCES tea_pitch (id)
);

create table tea_match_player
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    match_id    bigint                not null,
    team_id     bigint,
    user_id     bigint,
    PRIMARY KEY (id),
    CONSTRAINT tea_match_user_fk1 FOREIGN KEY (match_id) REFERENCES tea_match (id),
    CONSTRAINT tea_match_user_fk2 FOREIGN KEY (team_id) REFERENCES tea_team (id),
    CONSTRAINT tea_match_user_fk3 FOREIGN KEY (user_id) REFERENCES adm_user (id)
);

create table tea_match_player_stat
(
    id              bigint auto_increment not null,
    created_at      timestamp,
    created_by      bigint,
    modified_at     timestamp,
    modified_by     bigint,
    is_deleted      bool,
    match_player_id bigint                not null,
    code            varchar(255)          not null,
    value           text,
    stat_time       timestamp,
    PRIMARY KEY (id),
    CONSTRAINT tea_match_player_stat_fk1 FOREIGN KEY (match_player_id) REFERENCES tea_match_player (id)
);

create table tea_team_setting
(
    id                     bigint auto_increment not null,
    created_at             timestamp,
    created_by             bigint,
    modified_at            timestamp,
    modified_by            bigint,
    is_deleted             bool,
    team_id                bigint                not null,
    type                   varchar(255)          not null,
    sub_type               varchar(255),
    name                   varchar(255),
    description            text,
    amount_limit           decimal,
    training_pitch_id      bigint,
    training_schedule_type varchar(255),
    training_schedule_time float,
    PRIMARY KEY (id),
    CONSTRAINT tea_team_set_fk1 FOREIGN KEY (team_id) REFERENCES tea_team (id),
    CONSTRAINT tea_team_set_fk2 FOREIGN KEY (training_pitch_id) REFERENCES tea_pitch (id)
);

create table tea_team_fund
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    team_id     bigint                not null,
    balance     decimal               not null,
    PRIMARY KEY (id),
    CONSTRAINT tea_match_fund_fk1 FOREIGN KEY (team_id) REFERENCES tea_team (id)
);

create table tea_fund_transaction
(
    id               bigint auto_increment not null,
    created_at       timestamp,
    created_by       bigint,
    modified_at      timestamp,
    modified_by      bigint,
    is_deleted       bool,
    fund_id          bigint                not null,
    user_id          bigint,
    setting_id       bigint,
    transaction_type varchar(255),
    amount           decimal,
    note             text,
    paid_at          timestamp,
    status           int,
    PRIMARY KEY (id),
    CONSTRAINT tea_fund_tran_fk1 FOREIGN KEY (fund_id) REFERENCES tea_team_fund (id),
    CONSTRAINT tea_fund_tran_fk2 FOREIGN KEY (user_id) REFERENCES adm_user (id),
    CONSTRAINT tea_fund_tran_fk3 FOREIGN KEY (setting_id) REFERENCES tea_team_setting (id)
);

create table tea_team_event
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    team_id     bigint                not null,
    type        varchar(255)          not null,
    title       text,
    content     text,
    launch_at   timestamp,
    total_cost  decimal,
    PRIMARY KEY (id),
    CONSTRAINT tea_team_event_fk1 FOREIGN KEY (team_id) REFERENCES tea_team (id)
);

create table tea_event_attendance
(
    id          bigint auto_increment not null,
    created_at  timestamp,
    created_by  bigint,
    modified_at timestamp,
    modified_by bigint,
    is_deleted  bool,
    event_id    bigint                not null,
    user_id     bigint,
    is_attended bool,
    cost        decimal,
    PRIMARY KEY (id),
    CONSTRAINT tea_event_att_fk1 FOREIGN KEY (event_id) REFERENCES tea_team_event (id),
    CONSTRAINT tea_event_att_fk2 FOREIGN KEY (user_id) REFERENCES adm_user (id)
);
