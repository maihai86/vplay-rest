package vn.nodo.vplay.rest.security.token;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

public class FacebookAuthenticationToken extends UsernamePasswordAuthenticationToken {

    @Getter
    @Setter
    private LocalDateTime expiredTime;

    @Getter
    @Setter
    private List<String> permissions;

    @Getter
    @Setter
    private String fullName;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String avatarUrl;

    public FacebookAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public FacebookAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
