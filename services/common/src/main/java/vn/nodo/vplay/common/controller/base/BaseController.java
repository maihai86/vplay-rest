package vn.nodo.vplay.common.controller.base;

import vn.nodo.vplay.rest.service.base.BaseService;

public abstract class BaseController {

    public abstract BaseService getService();
}
