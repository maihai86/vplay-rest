package vn.nodo.vplay.rest.repository.content;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.rest.entity.content.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
}
