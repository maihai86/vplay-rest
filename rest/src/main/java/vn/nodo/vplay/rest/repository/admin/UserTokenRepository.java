package vn.nodo.vplay.rest.repository.admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.rest.entity.admin.UserToken;

@Repository
public interface UserTokenRepository extends JpaRepository<UserToken, Long> {
}
