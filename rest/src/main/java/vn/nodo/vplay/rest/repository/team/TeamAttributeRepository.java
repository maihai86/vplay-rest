package vn.nodo.vplay.rest.repository.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.rest.entity.team.TeamAttribute;

@Repository
public interface TeamAttributeRepository extends JpaRepository<TeamAttribute, Long> {
}
