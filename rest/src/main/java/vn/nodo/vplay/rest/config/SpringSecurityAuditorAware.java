package vn.nodo.vplay.rest.config;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import vn.nodo.vplay.rest.security.token.CustomUsernamePasswordAuthenticationToken;

import java.util.Optional;

public class SpringSecurityAuditorAware implements AuditorAware<Long> {
    @Override
    public Optional<Long> getCurrentAuditor() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .filter(Authentication::isAuthenticated)
                .filter(authentication -> authentication instanceof OAuth2Authentication)
                .map(authentication -> ((OAuth2Authentication) authentication).getUserAuthentication())
                .filter(authentication -> authentication instanceof CustomUsernamePasswordAuthenticationToken)
                .map(authentication -> ((CustomUsernamePasswordAuthenticationToken) authentication).getId());
    }
}
