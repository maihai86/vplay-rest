-- liquibase formatted sql
-- changeset haimt:03

create table cnt_news
(
    id           bigint auto_increment not null,
    created_at   timestamp,
    created_by   bigint,
    modified_at  timestamp,
    modified_by  bigint,
    is_deleted   bool,
    type         varchar(255)          not null,
    title_type   varchar(255),
    title        text,
    content_type varchar(255),
    content      text,
    PRIMARY KEY (id)
);

create table cnt_event
(
    id           bigint auto_increment not null,
    created_at   timestamp,
    created_by   bigint,
    modified_at  timestamp,
    modified_by  bigint,
    is_deleted   bool,
    type         varchar(255)          not null,
    title_type   varchar(255),
    title        text,
    content_type varchar(255),
    content      text,
    launch_at    timestamp,
    PRIMARY KEY (id)
);