package vn.nodo.vplay.common.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Util {

    public static final DateFormat orderCodeDf = new SimpleDateFormat("yyMMdd");
    public static final DateFormat dateOnlyDf = new SimpleDateFormat("dd/MM/yyyy");

    public static String buildAgentOrderCode(Long agentId, Long orderCodeIdx) {
        return "DL" + String.format("%03d", agentId) + orderCodeDf.format(new Date()) + String.format("%06d", orderCodeIdx);
    }

    public static Timestamp startDateConvert(LocalDate startDate) {
        return startDate != null
                ? Timestamp.valueOf(startDate.atTime(0, 0, 0)) : new Timestamp(System.currentTimeMillis());
    }

    public static Timestamp endDateConvert(LocalDate endDate) {
        return endDate != null
                ? Timestamp.valueOf(endDate.atTime(23, 59, 59)) : new Timestamp(System.currentTimeMillis());
    }

    public static String stringConvert(String keyword) {
        return keyword == null || keyword.isEmpty() ? null : keyword.trim().toUpperCase();
    }

    public static String buildSearchCriteria(String source) {
        if (source == null) return source;

        while (source.contains("  ")) {
            source = source.replaceAll(" {2}", " ");
        }

        return "%" + source.trim().replaceAll(" ", "%") + "%";
    }

    public static String formatDateOnly(Timestamp timestamp) {
        if (timestamp == null) return "";
        return dateOnlyDf.format(timestamp);
    }

    public static String formatCode(String source) {
        if (StringUtils.isBlank(source)) return source;

        String code = stripAccents(source).trim();
        while (code.contains("  ")) code = code.replaceAll("  ", " ");

        return code.replaceAll(" ", "_").toUpperCase();
    }

    public static BigDecimal nvl(BigDecimal source) {
        return source == null ? BigDecimal.ZERO : source;
    }

    public static String stripAccents(String source) {
        if (StringUtils.isBlank(source)) return source;

        return StringUtils.stripAccents(source)
                .replaceAll("đ", "d")
                .replaceAll("Đ", "D");
    }

    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (java.beans.PropertyDescriptor pd : pds) {
            //check if value of this property is null then add it to the collection
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    public static String getFileExtenstion(String filename) {
        if (StringUtils.isBlank(filename) || !filename.contains(".")) {
            return null;
        }
        return filename.substring(filename.lastIndexOf(".") + 1);
    }
}
