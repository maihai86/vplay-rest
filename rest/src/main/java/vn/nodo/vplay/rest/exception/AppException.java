package vn.nodo.vplay.rest.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppException extends RuntimeException {
    @Builder.Default
    private Integer httpStatus = 500;

    @Builder.Default
    private List<String> errorCodes = new ArrayList<>();

    @Builder.Default
    private List<AppExceptionError> errors = new ArrayList<>();
}
