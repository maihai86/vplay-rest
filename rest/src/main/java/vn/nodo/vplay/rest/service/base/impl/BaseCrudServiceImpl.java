package vn.nodo.vplay.rest.service.base.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.nodo.vplay.rest.entity.base.BaseEntity;
import vn.nodo.vplay.rest.exception.AppException;
import vn.nodo.vplay.rest.security.userdetails.CustomUserDetails;
import vn.nodo.vplay.rest.service.base.BaseCrudService;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Transactional
public abstract class BaseCrudServiceImpl<T extends BaseEntity, ID extends Serializable, A extends Serializable, E extends Serializable, S extends Serializable>
        extends BaseServiceImpl implements BaseCrudService<T, ID, A, E, S> {

    public abstract JpaRepository<T, ID> getRepository();

    @Override
    public T save(T entity) {
        return getRepository().save(entity);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public T findById(ID id) {
        return getRepository().findById(id).orElseThrow(() -> AppException.builder()
                .errorCodes(Collections.singletonList("error.common.entity-not-found"))
                .build());
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<T> findAllById(Iterable<ID> ids) {
        return getRepository().findAllById(ids);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PageImpl<T> findAll(Pageable pageable) {
        Page<T> page = getRepository().findAll(pageable);
        return new PageImpl<>(page.getContent(), page.getPageable(), page.getTotalElements());
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<T> findAll(Sort sort) {
        return getRepository().findAll(sort);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public boolean delete(Long userId, ID id) {
        T entity = getRepository().findById(id).orElseThrow(() -> AppException.builder()
                .errorCodes(Collections.singletonList("error.common.entity-not-found"))
                .build());
        entity.setDeleted(true);
        entity.setModifiedBy(userId);
        getRepository().save(entity);

        return true;
    }
}
