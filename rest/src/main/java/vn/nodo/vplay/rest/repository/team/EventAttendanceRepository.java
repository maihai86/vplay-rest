package vn.nodo.vplay.rest.repository.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.rest.entity.team.EventAttendance;

@Repository
public interface EventAttendanceRepository extends JpaRepository<EventAttendance, Long> {
}
