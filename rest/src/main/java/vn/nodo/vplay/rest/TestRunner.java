package vn.nodo.vplay.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Component
@Slf4j
public class TestRunner implements CommandLineRunner {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run(String... args) throws Exception {
        Map<String, Object> objectMap = applicationContext.getBeansWithAnnotation(Repository.class);
        log.info("FFFFFFFFFFFFFFFFFF inject total {} repository: {}", objectMap.size(), objectMap);
        objectMap.forEach((key, value) -> {
            if (value instanceof JpaRepository) {
                log.info("FFFFFFFFFFFFFFFFFF {}.findAll: {}", key, ((JpaRepository) value).findAll(PageRequest.of(0, 1)).getContent());
            }
        });
    }
}
