package vn.nodo.vplay.admin;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"vn.nodo.vplay.common", "vn.nodo.vplay.admin"})
public class AdminModule {
}
