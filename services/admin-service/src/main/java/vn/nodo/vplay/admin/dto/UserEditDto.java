package vn.nodo.vplay.admin.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vn.nodo.vplay.common.dto.base.BaseDto;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@ToString
public class UserEditDto extends BaseDto {
}
