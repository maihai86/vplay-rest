package vn.nodo.vplay.admin.service;

import vn.nodo.vplay.rest.dto.admin.PermissionAddDto;
import vn.nodo.vplay.rest.dto.admin.PermissionEditDto;
import vn.nodo.vplay.rest.dto.admin.PermissionSearchDto;
import vn.nodo.vplay.common.entity.admin.Permission;
import vn.nodo.vplay.rest.service.base.BaseCrudService;

public interface PermissionService extends BaseCrudService<Permission, Long, PermissionAddDto, PermissionEditDto, PermissionSearchDto> {
}
