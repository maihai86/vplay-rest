package vn.nodo.vplay.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.common.entity.admin.UserToken;

@Repository
public interface UserTokenRepository extends JpaRepository<UserToken, Long> {
}
