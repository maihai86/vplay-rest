package vn.nodo.vplay.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import vn.nodo.vplay.rest.security.token.CustomUsernamePasswordAuthenticationToken;

import java.security.Principal;

@Api(tags = "Principal Controller")
@RestController
@RequestMapping("api/v1/principals")
@Slf4j
public class PrincipalController {

    @ApiOperation("Test me")
    @GetMapping("me")
    /*@PreAuthorize("hasAuthority('SUPER_ADMIN')")*/
    public Principal me(@ApiIgnore @AuthenticationPrincipal final OAuth2Authentication principal) {
        Assert.notNull(principal.getUserAuthentication(), "userAuthentication must not be null");
        Assert.isInstanceOf(CustomUsernamePasswordAuthenticationToken.class, principal.getUserAuthentication());
        log.info("me, id: {}", ((CustomUsernamePasswordAuthenticationToken) principal.getUserAuthentication()).getId());
        return principal;
    }

    @ApiOperation("Test me (OTHER authority)")
    @GetMapping("me/other")
    @PreAuthorize("hasAuthority('OTHER')")
    public Principal me2(@ApiIgnore @AuthenticationPrincipal final OAuth2Authentication principal) {
        return principal;
    }

    @ApiOperation("Test me Permit all")
    @GetMapping("me/permit-all")
    public Principal me3(@ApiIgnore @AuthenticationPrincipal final OAuth2Authentication principal) {
        return principal;
    }
}
