package vn.nodo.vplay.rest.entity.admin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import vn.nodo.vplay.rest.entity.base.BaseEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "adm_user")
@Where(clause = "is_deleted = false")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EntityListeners(AuditingEntityListener.class)
public class User extends BaseEntity {

    @Column(name = "is_system")
    protected Boolean system;

    @Column
    protected Integer type;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    @JsonIgnore
    private String password;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column
    private String email;

    @Column
    @Lob
    private String address;

    @Column
    private Integer gender;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "avatar_url")
    private String avatarUrl;

    @Column
    private Integer status;

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
    private List<UserRole> userRoles = new ArrayList<>();

    @PrePersist
    @Override
    public void prePersist() {
        super.prePersist();
        if (this.type == null) {
            this.type = UserType.VPLAY.getValue();
        }
        this.system = false;
    }

    @AllArgsConstructor
    public enum UserType {
        NA(0),
        VPLAY(1),
        FACEBOOK(2),
        ;

        @Getter
        private final int value;
    }

    @AllArgsConstructor
    public enum UserStatus {
        DISABLED(0),
        ACTIVE(1),
        NOT_ACTIVE(2),
        ;

        @Getter
        private final int value;
    }
}
