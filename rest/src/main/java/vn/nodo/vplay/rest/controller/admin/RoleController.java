package vn.nodo.vplay.rest.controller.admin;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.nodo.vplay.rest.controller.base.BaseCrudController;
import vn.nodo.vplay.rest.dto.admin.RoleAddDto;
import vn.nodo.vplay.rest.dto.admin.RoleEditDto;
import vn.nodo.vplay.rest.dto.admin.RoleSearchDto;
import vn.nodo.vplay.rest.service.admin.RoleService;
import vn.nodo.vplay.rest.entity.admin.Role;
import vn.nodo.vplay.rest.service.base.BaseCrudService;

@Api(tags = "Role Controller")
@RestController
@RequestMapping("api/v1/roles")
public class RoleController extends BaseCrudController<Role, Long, RoleAddDto, RoleEditDto, RoleSearchDto> {

    @Autowired
    private RoleService roleService;

    @Override
    public BaseCrudService<Role, Long, RoleAddDto, RoleEditDto, RoleSearchDto> getService() {
        return roleService;
    }
}
