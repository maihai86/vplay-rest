package vn.nodo.vplay.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.common.entity.admin.NotificationConfig;

@Repository
public interface NotificationConfigRepository extends JpaRepository<NotificationConfig, Long> {
}
