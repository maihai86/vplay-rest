package vn.nodo.vplay.rest.service.admin.impl;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.nodo.vplay.rest.dto.admin.UserAddDto;
import vn.nodo.vplay.rest.dto.admin.UserEditDto;
import vn.nodo.vplay.rest.dto.admin.UserSearchDto;
import vn.nodo.vplay.rest.repository.admin.RoleRepository;
import vn.nodo.vplay.rest.repository.admin.UserRepository;
import vn.nodo.vplay.rest.security.userdetails.CustomUserDetails;
import vn.nodo.vplay.rest.service.admin.UserService;
import vn.nodo.vplay.rest.entity.admin.Role;
import vn.nodo.vplay.rest.entity.admin.User;
import vn.nodo.vplay.rest.entity.admin.UserRole;
import vn.nodo.vplay.rest.exception.AppException;
import vn.nodo.vplay.rest.service.base.impl.BaseCrudServiceImpl;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl extends BaseCrudServiceImpl<User, Long, UserAddDto, UserEditDto, UserSearchDto> implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public JpaRepository<User, Long> getRepository() {
        return userRepository;
    }

    @Override
    public User create(Long userId, UserAddDto data) {
        throw AppException.builder()
                .errorCodes(Collections.singletonList("error.not-implemented-yet"))
                .build();
    }

    @Override
    public User update(Long userId, Long id, UserEditDto data) {
        throw AppException.builder()
                .errorCodes(Collections.singletonList("error.not-implemented-yet"))
                .build();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PageImpl<User> findAll(UserSearchDto data, Pageable pageable) {
        Page<User> page = userRepository.findAll(pageable);
        return new PageImpl<>(page.getContent(), page.getPageable(), page.getTotalElements());
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<User> findAll(UserSearchDto data, Sort sort) {
        return userRepository.findAll(sort);
    }

    @Override
    public User createByMobile(UserAddDto data) {
        if (userRepository.findFirstByUsernameAndTypeOrderByCreatedAtDesc(data.getUsername(), User.UserType.VPLAY.getValue()).isPresent()) {
            throw AppException.builder()
                    .errorCodes(Collections.singletonList("error.admin.user-already-existed"))
                    .build();
        }
        Role role = roleRepository.findFirstByCodeOrderByCreatedAtDesc(Role.RoleCode.ROLE_USER.getValue())
                .orElseThrow(() -> AppException.builder()
                        .errorCodes(Collections.singletonList("error.admin.role-not-found"))
                        .build());

        User user = User.builder()
                .type(User.UserType.VPLAY.getValue())
                .username(data.getUsername())
                .password(passwordEncoder.encode(data.getPassword()))
                .mobileNumber(data.getUsername())
                .status(User.UserStatus.ACTIVE.getValue()) // TODO test
                .build();
        user.setUserRoles(Collections.singletonList(UserRole.builder().user(user).role(role).build()));

        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public User getCurrentUser(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> AppException.builder()
                .errorCodes(Collections.singletonList("error.admin.user-already-existed"))
                .build());
        Hibernate.initialize(user.getUserRoles());
        user.getUserRoles().forEach(userRole -> {
            Hibernate.initialize(userRole.getRole());
            Hibernate.initialize(userRole.getTeam());
            if (userRole.getRole() != null) {
                Hibernate.initialize(userRole.getRole().getPermissions());
            }
        });
        return user;
    }
}
