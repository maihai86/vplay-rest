package vn.nodo.vplay.rest.service.base.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import vn.nodo.vplay.rest.service.base.CacheService;

@Service
public class CacheServiceImpl implements CacheService {

    @Autowired
    private CacheManager cacheManager;

    @Override
    public void evictAllCaches() {
        cacheManager.getCacheNames()
                .forEach(cacheName -> {
                    if (cacheManager.getCache(cacheName) != null) {
                        cacheManager.getCache(cacheName).clear();
                    }
                });
    }
}
