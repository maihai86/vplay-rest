package vn.nodo.vplay.rest.security.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import vn.nodo.vplay.rest.security.token.CustomUsernamePasswordAuthenticationToken;
import vn.nodo.vplay.rest.security.userdetails.CustomUserDetails;

import java.util.Map;

@Slf4j
public class CustomUserAuthenticationConverter extends DefaultUserAuthenticationConverter {

    final String USER_ID = "user_id";

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        Map<String, Object> result = (Map<String, Object>) super.convertUserAuthentication(authentication);
        result.put(USER_ID, ((CustomUserDetails) authentication.getPrincipal()).getId());

        return result;
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        Authentication authentication = super.extractAuthentication(map);
        if (map.containsKey(USER_ID)) {
            CustomUsernamePasswordAuthenticationToken result = CustomUsernamePasswordAuthenticationToken
                    .from((UsernamePasswordAuthenticationToken) authentication);
            result.setId(Long.parseLong(map.get(USER_ID).toString()));

            return result;
        }
        return authentication;
    }
}
