package vn.nodo.vplay.rest.security.token;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class CustomUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

    @Getter
    @Setter
    private Long id;

    public CustomUsernamePasswordAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public CustomUsernamePasswordAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    public static CustomUsernamePasswordAuthenticationToken from(UsernamePasswordAuthenticationToken token) {
        return new CustomUsernamePasswordAuthenticationToken(token.getPrincipal(), token.getCredentials(), token.getAuthorities());
    }
}
