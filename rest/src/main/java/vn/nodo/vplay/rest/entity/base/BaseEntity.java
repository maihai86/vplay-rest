package vn.nodo.vplay.rest.entity.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.io.Serializable;
import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@MappedSuperclass
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Slf4j
public abstract class BaseEntity implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "created_at")
    @CreatedDate
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    protected Instant createdAt;

    @Column(name = "created_by")
    @CreatedBy
    protected Long createdBy;

    @Column(name = "modified_at")
    @LastModifiedDate
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    protected Instant modifiedAt;

    @Column(name = "modified_by")
    @LastModifiedBy
    protected Long modifiedBy;

    @Column(name = "is_deleted")
    protected Boolean deleted;

    @PrePersist
    public void prePersist() {
        this.deleted = false;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BaseEntity) || ((BaseEntity) obj).getId() == null || getId() == null) {
            return super.equals(obj);
        }
        return ((BaseEntity) obj).getId().longValue() == getId().longValue();
    }

    @Override
    public int hashCode() {
        if (getId() == null) {
            return super.hashCode();
        }
        return getId().hashCode();
    }
}
