package vn.nodo.vplay.common.service.base;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import vn.nodo.vplay.common.entity.base.BaseEntity;
import vn.nodo.vplay.common.security.base.CustomUserDetails;

import java.io.Serializable;
import java.util.List;

public interface BaseCrudService<T extends BaseEntity, ID extends Serializable, A extends Serializable, E extends Serializable, S extends Serializable>
        extends BaseService {

    T create(CustomUserDetails principal, A data);

    T update(CustomUserDetails principal, ID id, E data);

    PageImpl<T> findAll(S data, Pageable pageable);

    List<T> findAll(S data, Sort sort);

    T save(T entity) throws Exception;

    T findById(ID id);

    List<T> findAllById(Iterable<ID> ids);

    PageImpl<T> findAll(Pageable pageable);

    List<T> findAll(Sort sort);

    List<T> findAll();

    boolean delete(CustomUserDetails principal, ID id);
}
