package vn.nodo.vplay.rest.repository.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.rest.entity.team.Pitch;

@Repository
public interface PitchRepository extends JpaRepository<Pitch, Long> {
}
