package vn.nodo.vplay.rest.service.base;

public interface CacheService {

    void evictAllCaches();
}
