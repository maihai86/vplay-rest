package vn.nodo.vplay.rest.security.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ConfigurationProperties("app.oauth2")
public class Oauth2ClientsProperties {

    @Getter
    @Setter
    private Map<String, ClientConfiguration> clients = new HashMap<>();

    public static class ClientConfiguration {

        @Getter
        @Setter
        private String clientId;

        @Getter
        @Setter
        private String clientSecret;

        @Getter
        @Setter
        private List<String> scopes;

        @Getter
        @Setter
        private List<String> grantTypes;

        @Getter
        @Setter
        private List<String> redirectUris;

        @Getter
        @Setter
        private Integer tokenValidity;

        @Getter
        @Setter
        private Integer tokenRefreshValidity;
    }
}
