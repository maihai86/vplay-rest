package vn.nodo.vplay.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vn.nodo.vplay.rest.dto.base.BaseDto;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class FacebookLoginDto extends BaseDto {

    @NotEmpty
    private String accessToken;

    @NotEmpty
    private String userId;

    private LocalDateTime expiredTime;

    private List<String> permissions;

    private String fullName;

    private String email;

    private String avatarUrl;

}
