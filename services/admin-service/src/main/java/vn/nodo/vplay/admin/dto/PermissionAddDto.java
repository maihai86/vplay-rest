package vn.nodo.vplay.admin.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vn.nodo.vplay.common.dto.base.BaseDto;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@ToString
public class PermissionAddDto extends BaseDto {

    @NotEmpty
    private String code;

    private String name;

    private String description;

}
