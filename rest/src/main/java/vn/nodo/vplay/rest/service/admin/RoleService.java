package vn.nodo.vplay.rest.service.admin;

import vn.nodo.vplay.rest.dto.admin.RoleAddDto;
import vn.nodo.vplay.rest.dto.admin.RoleEditDto;
import vn.nodo.vplay.rest.dto.admin.RoleSearchDto;
import vn.nodo.vplay.rest.entity.admin.Role;
import vn.nodo.vplay.rest.service.base.BaseCrudService;

public interface RoleService extends BaseCrudService<Role, Long, RoleAddDto, RoleEditDto, RoleSearchDto> {
}
