package vn.nodo.vplay.rest.dto.admin;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vn.nodo.vplay.rest.dto.base.BaseDto;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@ToString
public class UserSearchDto extends BaseDto {
}
