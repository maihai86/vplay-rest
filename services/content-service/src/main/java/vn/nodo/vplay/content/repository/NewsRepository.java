package vn.nodo.vplay.content.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.common.entity.content.News;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {
}
