package vn.nodo.vplay.rest.repository.admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nodo.vplay.rest.entity.admin.Role;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findFirstByCodeOrderByCreatedAtDesc(String role);
}
