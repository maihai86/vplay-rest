package vn.nodo.vplay.common.entity.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@SuperBuilder
public abstract class BaseAttribute extends BaseEntity {

    @Column
    protected String code;

    @Column
    protected String name;

    @Column
    protected String value;

    @Column(name = "value_type")
    protected String valueType;

    @Column
    @Lob
    protected String description;
}
