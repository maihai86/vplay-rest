package vn.nodo.vplay.admin.service;

import vn.nodo.vplay.rest.dto.admin.UserAddDto;
import vn.nodo.vplay.rest.dto.admin.UserEditDto;
import vn.nodo.vplay.rest.dto.admin.UserSearchDto;
import vn.nodo.vplay.common.entity.admin.User;
import vn.nodo.vplay.rest.service.base.BaseCrudService;

public interface UserService extends BaseCrudService<User, Long, UserAddDto, UserEditDto, UserSearchDto> {

    User createByMobile(UserAddDto data);

    User getCurrentUser(Long id);
}
