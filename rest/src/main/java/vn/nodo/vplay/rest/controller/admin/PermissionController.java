package vn.nodo.vplay.rest.controller.admin;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.nodo.vplay.rest.controller.base.BaseCrudController;
import vn.nodo.vplay.rest.dto.admin.PermissionAddDto;
import vn.nodo.vplay.rest.dto.admin.PermissionEditDto;
import vn.nodo.vplay.rest.dto.admin.PermissionSearchDto;
import vn.nodo.vplay.rest.service.admin.PermissionService;
import vn.nodo.vplay.rest.entity.admin.Permission;
import vn.nodo.vplay.rest.service.base.BaseCrudService;

@Api(tags = "Permission Controller")
@RestController
@RequestMapping("api/v1/permissions")
public class PermissionController extends BaseCrudController<Permission, Long, PermissionAddDto, PermissionEditDto, PermissionSearchDto> {

    @Autowired
    private PermissionService permissionService;

    @Override
    public BaseCrudService<Permission, Long, PermissionAddDto, PermissionEditDto, PermissionSearchDto> getService() {
        return permissionService;
    }
}
