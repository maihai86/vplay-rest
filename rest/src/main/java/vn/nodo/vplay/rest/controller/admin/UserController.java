package vn.nodo.vplay.rest.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import vn.nodo.vplay.rest.controller.base.BaseCrudController;
import vn.nodo.vplay.rest.dto.admin.UserAddDto;
import vn.nodo.vplay.rest.dto.admin.UserEditDto;
import vn.nodo.vplay.rest.dto.admin.UserSearchDto;
import vn.nodo.vplay.rest.security.userdetails.CustomUserDetails;
import vn.nodo.vplay.rest.service.admin.UserService;
import vn.nodo.vplay.rest.entity.admin.User;
import vn.nodo.vplay.rest.exception.AppException;
import vn.nodo.vplay.rest.response.AppResponseDto;
import vn.nodo.vplay.rest.service.base.BaseCrudService;
import vn.nodo.vplay.rest.security.token.CustomUsernamePasswordAuthenticationToken;

import javax.validation.Valid;

@Api(tags = "User Controller")
@RestController
@RequestMapping("api/v1/users")
public class UserController extends BaseCrudController<User, Long, UserAddDto, UserEditDto, UserSearchDto> {

    @Autowired
    private UserService userService;

    @Override
    public BaseCrudService<User, Long, UserAddDto, UserEditDto, UserSearchDto> getService() {
        return userService;
    }

    @ApiOperation(value = "Thêm", hidden = true)
    @Override
    @Deprecated
    public AppResponseDto<User> create(@ApiIgnore @AuthenticationPrincipal OAuth2Authentication principal, @Valid UserAddDto data) {
        throw AppException.builder().httpStatus(400).build();
    }

    @ApiOperation("Đăng ký SĐT")
    @PostMapping("register/mobile")
    public User registerMobile(@Valid @RequestBody UserAddDto data) {
        return userService.createByMobile(data);
    }

    @ApiOperation("Me")
    @GetMapping("me")
    public User me(@ApiIgnore @AuthenticationPrincipal OAuth2Authentication principal) {
        return userService.getCurrentUser(((CustomUsernamePasswordAuthenticationToken) principal.getUserAuthentication()).getId());
    }
}
